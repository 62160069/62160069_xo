
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author BmWz
 */
public class OXProject {

    static char winner = '_';
    static int turn = 0;
    static char[][] table = {
        {'_', '_', '_'},
        {'_', '_', '_'},
        {'_', '_', '_'}
    };
    static char player = 'X';
    static int row, col;
    static Scanner kb = new Scanner(System.in);
    static boolean isFinsh = false;

    static void showWelcome() {
        System.out.println("Welcome  to OX Game");
    }

    static void showTable() {
        System.out.println(" 1 2 3");
        for (int row = 0; table.length > row; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col] + " ");
            }
            System.out.println();
        }
    }

    static void showTurn() {
        System.out.println(player + " Turn");
        turn++;
    }

    static void input() {
        while (true) {
            System.out.println("Please input Row Col : ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if (table[row][col] == '_') {
                table[row][col] = player;
                break;
            }
            System.out.println("Error : table at row and col is not empty!!!!");
        }

    }

    static void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinsh = true;
        winner = player;
    }

    static void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinsh = true;
        winner = player;
    }

    static void checkCross() {
        if ((table[0][0] == player && table[1][1] == player && table[2][2] == player)
                || (table[0][2] == player && table[1][1] == player && table[2][0] == player)) {
            isFinsh = true;
            winner = player;
        }
    }

    static void checkDrew() {
        if (turn == 9) {
            winner = '_';
            isFinsh = true;
        }
    }

    static void checkWin() {
        checkCol();
        checkRow();
        checkCross();
        checkDrew();
    }

    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }

    static void showResult() {
        showTable();
        if (winner == '_') {
            System.out.println("------Drew------");
        } else {
            System.out.println(winner + " Win!!!");
        }
    }

    static void showBye() {
        System.out.println("Bye bye ...");
    }

    public static void main(String[] args) {
        showWelcome();
        do {
            showTable();
            showTurn();
            input();
            checkWin();
            switchPlayer();
            System.out.println(turn);
        } while (!isFinsh);
        showResult();
        showBye();
    }
}
